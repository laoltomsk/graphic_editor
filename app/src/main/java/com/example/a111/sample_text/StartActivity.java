package com.example.a111.sample_text;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    Button galleryButton, cameraButton;
    private static final int READ_REQUEST_CODE = 42;
    private static final int REQUEST_IMAGE_CAPTURE = 43;
    Uri photoUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        galleryButton = (Button) findViewById(R.id.start_button_gallery);
        galleryButton.setOnClickListener(this);
        cameraButton = (Button) findViewById(R.id.start_button_camera);
        cameraButton.setOnClickListener(this);
    }

    /** Создаёт временный файл для фотографии с камеры */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("HH-mm-ss_dd-MM-yy").format(new Date());
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile("camera_"+timeStamp, ".jpg", storageDir);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        //при клике на кнопку галереи
        if (id == R.id.start_button_gallery) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, READ_REQUEST_CODE);
        }

        //при клике на кнопку камеры
        if (id == R.id.start_button_camera) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                }
                catch (IOException ex) {}
                if (photoFile != null) {
                    photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                } else {
                    Toast.makeText(getApplicationContext(), "Не удалось запустить камеру", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        //при получении изображения из галереи
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri imageUri = data.getData();
                Intent i = new Intent(this, MainActivity.class);
                i.putExtra("imagePath", imageUri.toString());
                startActivity(i);
            }
        }

        //при получении изображения с камеры
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("imagePath", photoUri.toString());
            startActivity(i);
        }
    }
}
