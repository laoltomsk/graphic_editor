package com.example.a111.sample_text;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class RetouchActivity extends AppCompatActivity {

    Bitmap currentBitmap = MainActivity.currentBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retouch);
        final ImageView imageView = (ImageView) findViewById(R.id.retouch_imageview_main);
        imageView.setImageBitmap(MainActivity.currentBitmap);
        final TextView textUnderRetouchSeekbar = (TextView) findViewById(R.id.retouch_textview_current);

        Toast toast = Toast.makeText(getApplicationContext(), "Выберите радиус кисти", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        SeekBar retouchSeekBar = (SeekBar) findViewById(R.id.retouch_seekbar_thickness);
        retouchSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderRetouchSeekbar.setText("Радиус кисти: " + (progress+1));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        imageView.setOnTouchListener(new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_MOVE) { //в момент прекращения касания
                //сохранение координат
                double imageViewWidth = imageView.getWidth();
                double scale = currentBitmap.getWidth() / imageViewWidth;
                int x = (int)(event.getX()*scale);
                int y = (int)(event.getY()*scale);

                //немного инициализации
                SeekBar retouchSeekBar = (SeekBar) findViewById(R.id.retouch_seekbar_thickness);
                int radius = retouchSeekBar.getProgress()+1;
                int matrixSize = 2 * radius + 1;
                int x1=0, y1=0;
                double matrix [][] = new double[matrixSize][matrixSize];

                //заполнение матрицы по изменённой формуле Гаусса
                for (int i = -radius; i <= radius; i++)
                {
                    for (int j = -radius; j <= radius; j++)
                    {
                        double xDouble = (double)i, yDouble = (double)j, radiusDouble = (double)radius;
                        matrix[x1][y1] = (Math.pow (Math.E, -((xDouble*xDouble+yDouble*yDouble)/(2*radiusDouble*radiusDouble))))*0.25;
                        y1++;
                    }
                    y1=0;
                    x1++;
                }

                //ещё немного инициализации
                int bitmapWidth = currentBitmap.getWidth();
                int bitmapHeight = currentBitmap.getHeight();
                if (x < 0 || y < 0 || x > bitmapWidth || y > bitmapHeight) return true; //игнор касаний за пределами битмапа (вдруг?)
                int[] bitmapPixels = new int[bitmapWidth * bitmapHeight];
                int averageRed = 0, averageGreen = 0, averageBlue = 0, circleCount = 0;
                currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

                //подсчёт среднего цвета пикселя
                for (int i = x-radius; i <= x+radius; i++) {
                    for (int j = y-radius; j <= y+radius; j++) {
                        if ((int)Math.sqrt((x-i)*(x-i)+(y-j)*(y-j)) <= radius && i > 0 && j > 0 && i < bitmapWidth && j < bitmapHeight) {
                            if(Color.alpha(bitmapPixels[j*bitmapWidth+i]) != 0) {
                                averageRed += Color.red(bitmapPixels[j * bitmapWidth + i]);
                                averageGreen += Color.green(bitmapPixels[j * bitmapWidth + i]);
                                averageBlue += Color.blue(bitmapPixels[j * bitmapWidth + i]);
                                circleCount++;
                            }
                        }
                    }
                }
                if(circleCount !=0) {
                    averageRed /= circleCount;
                    averageGreen /= circleCount;
                    averageBlue /= circleCount;
                }
                //обновление цветов пикселей с учётом среднего цвета и матрицы конволюции
                x1=0; y1=0;
                for (int i = x-radius; i <= x+radius; i++) {
                    for (int j = y-radius; j <= y+radius; j++) {
                        if ((int)Math.sqrt((x-i)*(x-i)+(y-j)*(y-j)) <= radius && i > 0 && j > 0 && i < bitmapWidth && j < bitmapHeight) {
                            if(Color.alpha(bitmapPixels[j*bitmapWidth+i]) != 0) {
                                int newRed = (int) (averageRed * matrix[x1][y1]) + (int) (Color.red(bitmapPixels[j * bitmapWidth + i]) * (1 - matrix[x1][y1]));
                                int newGreen = (int) (averageGreen * matrix[x1][y1]) + (int) (Color.green(bitmapPixels[j * bitmapWidth + i]) * (1 - matrix[x1][y1]));
                                int newBlue = (int) (averageBlue * matrix[x1][y1]) + (int) (Color.blue(bitmapPixels[j * bitmapWidth + i]) * (1 - matrix[x1][y1]));
                                bitmapPixels[j * bitmapWidth + i] = 255 * 16777216 + newRed * 65536 + newGreen * 256 + newBlue;
                                y1++;
                            }
                        }
                    }
                    y1=0;
                    x1++;
                }
                currentBitmap = Bitmap.createBitmap(bitmapPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
                imageView.setImageBitmap(currentBitmap);
            }
            return true;
        }
    });
    }

    /** сохранение изменений по нажатию ОК */
    public void save(View v) {
        int id = v.getId();
        if (id == R.id.retouch_button_ok) {
            MainActivity.currentBitmap = currentBitmap;
            super.onBackPressed();
        }
    }


}
