package com.example.a111.sample_text;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.HOGDescriptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static Bitmap currentBitmap;
    public static ImageView imageView;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.main_imageview_main);
        setSupportActionBar(toolbar);
        Bitmap image;

        //получение интента, адреса картинки
        Intent i = getIntent();
        String imagePath = i.getStringExtra("imagePath");
        Uri imageUri = Uri.parse(imagePath);

        //декодирование картинки со сжатием до 1000х1000
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        try {
            image = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, bmOptions);
        }
        catch (FileNotFoundException e) { //если картинка не найдена
            image = null;
        }
        int scaleFactor = Math.min(bmOptions.outWidth/1000, bmOptions.outHeight/1000);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor; //настройка сжатия
        bmOptions.inPurgeable = true;
        try {
            image = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, bmOptions);
        }
        catch (FileNotFoundException e) { //если картинка не найдена
            image = null;
        }
        imageView.setImageBitmap(image);
        currentBitmap = image;

        //про боковую панель (написано студией, не нами)
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    class KuwaharaTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE );
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int rad = params[0].intValue();
            currentBitmap = EditorFeatures.kuwaharaFilter(rad);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ImageView imageview = (ImageView) findViewById(R.id.main_imageview_main);
            imageview.setImageBitmap(currentBitmap);

            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            showSaveSnackbar();
        }
    }

    class UnsharpTaskParams {
        int r, t;
        double a;

        UnsharpTaskParams (int r, int t, double a){
            this.r = r;
            this.t = t;
            this.a = a;
        }

    }
    class UnsharpTask extends AsyncTask<UnsharpTaskParams, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @Override
        protected Void doInBackground(UnsharpTaskParams... params) {
            int r = params[0].r;
            double a = params[0].a;
            int t = params[0].t;
            currentBitmap = EditorFeatures.unsharpMasking(r, a, t);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ImageView imageView = (ImageView) findViewById(R.id.main_imageview_main);
            imageView.setImageBitmap(currentBitmap);

            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            showSaveSnackbar();
        }
    }

    class GaussTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int blurRadius = params[0].intValue();
            currentBitmap = EditorFeatures.gaussianBlur(blurRadius);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ImageView imageview = (ImageView) findViewById(R.id.main_imageview_main);
            imageview.setImageBitmap(currentBitmap);

            ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            showSaveSnackbar();
        }
    }

    protected void onResume() {
        super.onResume();
        ImageView imageview = (ImageView) findViewById(R.id.main_imageview_main);
        imageview.setImageBitmap(currentBitmap);

        //Вызов загрузчика библиотеки
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };


    /** Реакция на кнопку назад - закрытие меню или, если закрыто, уход назад */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /** Реакция на нажатия пунктов бокового меню */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_triangles) {
            workWithTriangles();
        } else if (id == R.id.nav_zoom) {
            zoomInit();
        } else if (id == R.id.nav_rotate) {
            rotateInit();
        } else if (id == R.id.nav_contrast) {
            contrastStretchInit();
        } else if (id == R.id.nav_sepia) {
            sepiaInit();
        } else if (id == R.id.nav_negative) {
            negativeInit();
        } else if (id == R.id.nav_gamma) {
            gammaInit();
        } else if (id == R.id.nav_kuwahara) {
            kuwaharaInit();
        } else if (id == R.id.nav_save) {
            saveInit();
        } else if (id == R.id.nav_ex) {
            unsharpMaskingInit();
        } else if (id == R.id.nav_gauss) {
            gaussInit();
        } else if (id == R.id.nav_faces) {
            searchForPeople();
        } else if (id == R.id.nav_retouch) {
            Intent i = new Intent(this, RetouchActivity.class);
            startActivity(i);
    }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /** Предложение пользователю сохранить изображение */
    public void showSaveSnackbar() {
        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_rellayout_main);
        Snackbar saveSnackbar = Snackbar.make(relativeLayout, "Сохранить изображение?", Snackbar.LENGTH_LONG)
                .setAction("Да", saveSnackbarOnClickListener)
                .setActionTextColor(getResources().getColor(R.color.backgroundColor));
        View snackbarView = saveSnackbar.getView();
        snackbarView.setBackgroundColor((getResources().getColor(R.color.colorPrimaryDark)));
        saveSnackbar.show();
    }
    View.OnClickListener saveSnackbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            saveInit();
        }
    };


    /** Процедура работы с треугольниками */
    public static int numberOfPoints;
    public void workWithTriangles() {
        // инициализация
        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_rellayout_main);
        numberOfPoints = 0;

        //инициализация снекбаров
        final Snackbar firstPointSnackbar = Snackbar.make(findViewById(R.id.main_rellayout_main), "Назначьте три точки исходного треугольника [1-2-3]", Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null);
        View snackbarView = firstPointSnackbar.getView();
        snackbarView.setBackgroundColor((getResources().getColor(R.color.colorPrimaryDark)));
        final Snackbar lastPointSnackbar = Snackbar.make(findViewById(R.id.main_rellayout_main), "Назначьте три точки конечного треугольника [4-5-6]", Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null);
        View snackbarView1 = lastPointSnackbar.getView();
        snackbarView1.setBackgroundColor((getResources().getColor(R.color.colorPrimaryDark)));

        firstPointSnackbar.show();

        imageView.setOnTouchListener(new View.OnTouchListener() {
            TextView pointsView[] = new TextView[6];
            int pointsX[] = new int[6];
            int pointsY[] = new int[6];
            int pointsViewIds[] = new int[6];
            @Override
            public boolean onTouch(View v, MotionEvent event) { //при касании
                if (event.getAction() == MotionEvent.ACTION_UP) { //в момент прекращения касания
                    if (numberOfPoints < 6) { //если меньше шести точек
                        //сохранение координат:
                        int x = (int) event.getX();
                        int y = (int) event.getY();
                        pointsX[numberOfPoints] = x;
                        pointsY[numberOfPoints] = y;

                        //добавление циферки на экран:
                        pointsView[numberOfPoints] = new TextView(getApplicationContext());
                        int text = numberOfPoints + 1;
                        pointsView[numberOfPoints].setBackgroundResource(R.drawable.shape_oval);
                        pointsView[numberOfPoints].setTypeface(null, Typeface.BOLD);
                        pointsView[numberOfPoints].setTextSize(15);
                        pointsView[numberOfPoints].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        pointsView[numberOfPoints].setGravity(Gravity.CENTER);
                        pointsView[numberOfPoints].setText("" + text);
                        int id = View.generateViewId();
                        pointsView[numberOfPoints].setId(id);
                        pointsViewIds[numberOfPoints] = id;
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(x, y, 0, 0);
                        relativeLayout.addView(pointsView[numberOfPoints], layoutParams);

                        numberOfPoints++;
                    }

                    //обновление текста после ввода трёх точек
                    if (numberOfPoints == 3) {
                        firstPointSnackbar.dismiss();
                        lastPointSnackbar.show();
                    }

                    //запуск функции после шести точек
                    if (numberOfPoints == 6) {
                        lastPointSnackbar.dismiss();

                        try {
                            EditorFeatures.affineTransformation(pointsX,pointsY);
                            showSaveSnackbar();
                        }
                        catch(OutOfMemoryError e) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 0, 50);
                            toast.show();
                        }

                        //сохранение результата
                        imageView.setImageBitmap(currentBitmap);

                        //удаление точек с экрана
                        numberOfPoints++;
                        for (int i = 0; i < 6; i++) {
                            TextView curView = (TextView) findViewById(pointsViewIds[i]);
                            relativeLayout.removeView(curView);
                        }
                    }
                }
                return true;
            }
        });
    }

    /** Процедура вызова масштабирования */
    public void zoomInit() {
        //создание всплывающего окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Масштабирование");
        final View view = this.getLayoutInflater().inflate(R.layout.activity_zoom_selector, null);
        builder.setView(view);

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //считываем значение
                EditText inputView = (EditText) view.findViewById(R.id.zoom_edittext_main);
                double zoomValue;
                try {
                    zoomValue = Double.parseDouble(inputView.getText().toString());
                }
                catch(Exception e) {
                    zoomValue = 100;
                    Toast toast = Toast.makeText(getApplicationContext(), "Вы что-то не так ввели", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //вызов функции
                try {
                    EditorFeatures.zooming(zoomValue);
                    showSaveSnackbar();
                }
                catch(OutOfMemoryError | NegativeArraySizeException | ArrayIndexOutOfBoundsException e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //сохранение результатов
                imageView.setImageBitmap(currentBitmap);
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура вызова увеличения контраста */
    public void contrastStretchInit() {
        try {
            currentBitmap = EditorFeatures.contrastStretch();
            showSaveSnackbar();
        }
        catch(OutOfMemoryError e) { //если недостаточно памяти
            Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
        imageView.setImageBitmap(currentBitmap);
    }

    /** Процедура вызова фильтра сепии */
    public void sepiaInit() {
        try {
            currentBitmap = EditorFeatures.sepiaEffect();
            showSaveSnackbar();
        }
        catch(OutOfMemoryError e) { //если недостаточно памяти
            Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
        imageView.setImageBitmap(currentBitmap);
    }

    /** Процедура вызова фильтра негатива */
    public void negativeInit() {
        try {
            currentBitmap = EditorFeatures.negativeFilter();
            showSaveSnackbar();
        }
        catch(OutOfMemoryError e) { //если недостаточно памяти
            Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
        imageView.setImageBitmap(currentBitmap);
    }

    /** Процедура вызова фильтра гаммы */
    public void gammaInit() {
        //создание всплывающего окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Гамма");
        final View view = this.getLayoutInflater().inflate(R.layout.activity_gamma, null);
        builder.setView(view);
        final TextView textUnderGammaSeekBar = (TextView)view.findViewById(R.id.gamma_textview_current);
        SeekBar gammaSeekbar = (SeekBar)view.findViewById(R.id.gamma_seekbar_main);

        //обновление подписи под seekbar
        gammaSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderGammaSeekBar.setText("Выбранное значение: " + (progress/100.0));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //считываем значение
                SeekBar gammaSeekBar = (SeekBar) view.findViewById(R.id.gamma_seekbar_main);
                double gammaValue = (double) (gammaSeekBar.getProgress() + 1);
                gammaValue = gammaValue / 100.0;

                //вызов функции
                try {
                    currentBitmap = EditorFeatures.gammaFilter(gammaValue);
                    showSaveSnackbar();
                } catch (OutOfMemoryError e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //сохранение результатов
                imageView.setImageBitmap(currentBitmap);
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура вызова фильтра Кувахары */
    public void kuwaharaInit() {
        //создание всплывающего окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Радиус действия фильтра");
        final View view = this.getLayoutInflater().inflate(R.layout.activity_kuwahara, null);
        builder.setView(view);
        final TextView textUnderKuwaharaSeekbar = (TextView)view.findViewById(R.id.kuwahara_textview_main);
        SeekBar kuwaharaSeekbar = (SeekBar)view.findViewById(R.id.kuwahara_seekbar_main);

        //обновление подписи под seekbar
        kuwaharaSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderKuwaharaSeekbar.setText("Выбранное значение: " + (progress+1));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //считывание значения
                SeekBar kuwaharaSeekbar = (SeekBar) view.findViewById(R.id.kuwahara_seekbar_main);
                int kuwaharaRadius =  kuwaharaSeekbar.getProgress() + 1;

                //запуск функции
                try {
                    new KuwaharaTask().execute(new Integer (kuwaharaRadius));
                } catch (OutOfMemoryError e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //убивание всплывающего окна
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура вызова вращения */
    public void rotateInit() {
        //создание диалогового окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Вращение");
        final View view = this.getLayoutInflater().inflate(R.layout.activity_rotation_selector, null);
        builder.setView(view);

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //считывание значения
                EditText inputView = (EditText) view.findViewById(R.id.rotate_edittext_main);
                double rotateAngle = Double.parseDouble(inputView.getText().toString());

                //запуск функции
                try {
                    EditorFeatures.rotation(rotateAngle);
                    showSaveSnackbar();
                }
                catch(OutOfMemoryError e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //сохранение результата
                imageView.setImageBitmap(currentBitmap);
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура запуска сохранения: если разрешения нет, запрашивает, иначе запускает сохранение */
    public void saveInit() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            saving();
        }
    }

    /** Вызывается при отказе/согласии на выдачу разрешения. Запускает сохранение либо выдаёт ошибку */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            saving();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Без разрешения я ничего сохранить не могу", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
    }

    /** Процедура сохранения */
    public void saving() {
        try {
            //создание имени файла, определение папки
            String timeStamp = new SimpleDateFormat("HH-mm-ss_dd-MM-yy").format(new Date()); //генерация уникального имени
            String fileName = "edited_" + timeStamp + ".jpg";
            String storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            File saveTo = new File(storageDir, "photoEditor");
            saveTo.mkdirs(); //создаёт папку, если её ещё нет
            File file = new File(saveTo, fileName);

            //собственно сохранение
            FileOutputStream out = new FileOutputStream(file);
            currentBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

            //радостное уведомление
            Toast toast = Toast.makeText(getApplicationContext(), "Сохранено!", Toast.LENGTH_SHORT);   //уведомленьице
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** Процедура вызова Гауссова размытия */
    public void gaussInit() {
        //создание диалогового окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выберите радиус размытия");
        final View view = this.getLayoutInflater().inflate(R.layout.activity_gauss, null);
        builder.setView(view);
        final TextView textUnderGaussSeekbar = (TextView)view.findViewById(R.id.gauss_textview_current);
        SeekBar gaussSeekbar = (SeekBar)view.findViewById(R.id.gauss_seekbar_main);

        //обновление подписи под seekbar
        gaussSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderGaussSeekbar.setText("Выбранное значение: " + (progress+1));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //получение значения
                SeekBar gaussSeekbar = (SeekBar) view.findViewById(R.id.gauss_seekbar_main);
                int gaussRadius = gaussSeekbar.getProgress() + 1;

                //запуск функции
                try {
                    new GaussTask().execute(gaussRadius);
                }
                catch(OutOfMemoryError e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);   //уведомленьице
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //сохранение результата
                imageView.setImageBitmap(currentBitmap);
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура вызова нерезкого маскирования */
    public void unsharpMaskingInit() {
        //создание диалогового окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View view = this.getLayoutInflater().inflate(R.layout.activity_unsharp, null);
        builder.setView(view);
        final TextView textUnderRadius = (TextView)view.findViewById(R.id.unsharp_textview_radius_current);
        final TextView textUnderAmount = (TextView)view.findViewById(R.id.unsharp_textview_amount_current);
        final TextView textUnderThreshold = (TextView)view.findViewById(R.id.unsharp_textview_threshold_current);
        SeekBar unsharpRadiusSeekbar = (SeekBar)view.findViewById(R.id.unsharp_seekbar_radius);
        SeekBar unsharpAmountSeekbar = (SeekBar)view.findViewById(R.id.unsharp_seekbar_amount);
        SeekBar unsharpThresholdSeekbar = (SeekBar)view.findViewById(R.id.unsharp_seekbar_threshold);

        //обновление подписей под seekbar-ами
        unsharpRadiusSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            textUnderRadius.setText("Выбранное значение: " + (progress+1));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        unsharpAmountSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderAmount.setText("Выбранное значение: " + (progress/100.0));
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        unsharpThresholdSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                textUnderThreshold.setText("Выбранное значение: " + progress);
            }
            public void onStartTrackingTouch(SeekBar arg0) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //при нажатии на ОК
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //считывание значений
                SeekBar unsharpRadiusSeekbar = (SeekBar) view.findViewById(R.id.unsharp_seekbar_radius);
                int unsharpRadius = unsharpRadiusSeekbar.getProgress() + 1;
                SeekBar unsharpAmountSeekbar = (SeekBar) view.findViewById(R.id.unsharp_seekbar_amount);
                double unsharpAmount = (double)(unsharpAmountSeekbar.getProgress());
                unsharpAmount = unsharpAmount / 100.0;
                SeekBar unsharpThresholdSeekbar = (SeekBar) view.findViewById(R.id.unsharp_seekbar_threshold);
                int unsharpThreshold = unsharpThresholdSeekbar.getProgress();

                //запуск функции
                try {
                    UnsharpTaskParams t = new UnsharpTaskParams(unsharpRadius, unsharpThreshold, unsharpAmount);
                    new UnsharpTask().execute(t);
                }
                catch(OutOfMemoryError e) { //если недостаточно памяти
                    Toast toast = Toast.makeText(getApplicationContext(), "Недостаточно памяти для выполнения операции", Toast.LENGTH_SHORT);   //уведомленьице
                    toast.setGravity(Gravity.BOTTOM, 0, 50);
                    toast.show();
                }

                //сохранение результата
                imageView.setImageBitmap(currentBitmap);
                dialog.dismiss();
            }
        });

        //запуск диалогового окна
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /** Процедура поиска силуэтов людей */
    void searchForPeople() {
        //инициализация
        Mat sourceBitmapMat = new Mat();
        Mat grayBitmapMat = new Mat();
        Utils.bitmapToMat(currentBitmap, sourceBitmapMat);
        Imgproc.cvtColor(sourceBitmapMat, grayBitmapMat, Imgproc.COLOR_RGB2GRAY, 4); // Переконвертируем матрицу с RGB на градацию серого
        HOGDescriptor hog = new HOGDescriptor();
        MatOfFloat descriptors = HOGDescriptor.getDefaultPeopleDetector();
        hog.setSVMDetector(descriptors);
        MatOfRect locations = new MatOfRect();
        MatOfDouble weights = new MatOfDouble();

        //собственно поиск
        hog.detectMultiScale(grayBitmapMat, locations, weights);

        //построение прямоугольников
        Point rectPoint1 = new Point();
        Point rectPoint2 = new Point();
        if (locations.rows() > 0) {
            List<Rect> rectangles = locations.toList();
            for (Rect rect : rectangles) {
                //создание прямоугольника
                rectPoint1.x = rect.x;
                rectPoint1.y = rect.y;
                rectPoint2.x = rect.x + rect.width;
                rectPoint2.y = rect.y + rect.height;
                final Scalar rectColor = new Scalar(0, 0, 0);

                //добавление прямоугольника на экран
                Imgproc.rectangle(sourceBitmapMat, rectPoint1, rectPoint2, rectColor, 2);
            }
        }

        //сохранение результатов
        Utils.matToBitmap(sourceBitmapMat, currentBitmap);
        imageView.setImageBitmap(currentBitmap);
        showSaveSnackbar();
    }
}
