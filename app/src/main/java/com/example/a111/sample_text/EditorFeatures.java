package com.example.a111.sample_text;

import android.graphics.Bitmap;
import android.graphics.Color;

public class EditorFeatures {

    /** выполняет аффинное преобразование */
    public static void affineTransformation(int[] pointsX, int[] pointsY) {
        //вычисление элементов матрицы (мне тоже страшно от этого кода)
        double delta = pointsX[0] * pointsY[1] + pointsX[1] * pointsY[2] + pointsX[2] * pointsY[0] - pointsX[2] * pointsY[1] - pointsX[1] * pointsY[0] - pointsX[0] * pointsY[2];
        double deltaA11 = pointsX[3] * pointsY[1] + pointsX[4] * pointsY[2] + pointsX[5] * pointsY[0] - pointsX[5] * pointsY[1] - pointsX[4] * pointsY[0] - pointsX[3] * pointsY[2];
        double deltaA21 = pointsX[0] * pointsX[4] + pointsX[1] * pointsX[5] + pointsX[2] * pointsX[3] - pointsX[2] * pointsX[4] - pointsX[1] * pointsX[3] - pointsX[0] * pointsX[5];
        double deltaA12 = pointsY[3] * pointsY[1] + pointsY[4] * pointsY[2] + pointsY[5] * pointsY[0] - pointsY[5] * pointsY[1] - pointsY[4] * pointsY[0] - pointsY[3] * pointsY[2];
        double deltaA22 = pointsX[0] * pointsY[4] + pointsX[1] * pointsY[5] + pointsX[2] * pointsY[3] - pointsX[2] * pointsY[4] - pointsX[1] * pointsY[3] - pointsX[0] * pointsY[5];
        double a11 = deltaA11 / delta;
        double a21 = deltaA21 / delta;
        double a12 = deltaA12 / delta;
        double a22 = deltaA22 / delta;
        double detM = a11 * a22 - a12 * a21;

        //вычисление коэффициентов по числам в матрице, формулы с хабра
        double rotateAngle, stretchY, stretchX, moveX;
        if (a22 == 0) {
            rotateAngle = Math.PI / 2;
            stretchY = -a21;
        } else {
            rotateAngle = Math.atan(-a21 / a22);
            stretchY = a22 / Math.cos(rotateAngle);
        }
        stretchX = detM / stretchY;
        moveX = (a11 * a21 + a12 * a22) / detM;

        //инициализация переменных перед преобазованием
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

        //растяжение/сжатие
        int[] tempBitmapPixels;
        if (Math.abs(stretchX)*Math.abs(stretchY) > 1) {
            tempBitmapPixels = bilinearFiltering(stretchX, stretchY, bitmapWidth, bitmapHeight, bitmapPixels);
        } else {
            tempBitmapPixels = trilinearFiltering(stretchX, stretchY, bitmapWidth, bitmapHeight, bitmapPixels);
        }
        int tempBitmapWidth = Math.abs((int) (bitmapWidth * stretchX));
        int tempBitmapHeight = Math.abs((int) (bitmapHeight * stretchY));

        //сдвиг (почти всегда ничтожный)
        tempBitmapPixels = movePixels(moveX, 0, tempBitmapWidth, tempBitmapHeight, tempBitmapPixels);

        //поворот
        int newBitmapWidth = (int)(tempBitmapHeight * Math.abs(Math.sin(rotateAngle))
                + tempBitmapWidth * Math.abs(Math.cos(rotateAngle)));
        int newestBitmapHeight = (int)(tempBitmapHeight * Math.abs(Math.cos(rotateAngle))
                + tempBitmapWidth * Math.abs(Math.sin(rotateAngle)));
        int tempPixels[] = rotatePixels(-rotateAngle, tempBitmapWidth, tempBitmapHeight, newBitmapWidth, newestBitmapHeight, tempBitmapPixels);

        //обновление currentBitmap
        MainActivity.currentBitmap = Bitmap.createBitmap(tempPixels, newBitmapWidth, newestBitmapHeight, Bitmap.Config.ARGB_8888);
        MainActivity.currentBitmap = killWhitespace(MainActivity.currentBitmap, tempPixels, newBitmapWidth, newestBitmapHeight);
    }

    /** масштабирует изображение */
    public static void zooming(double zoomValue) {
        //инициализация переменных перед преобазованием
        zoomValue /= 100;
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

        //растяжение/сжатие
        int[] newBitmapPixels;
        if (Math.abs(zoomValue) > 1) {
            newBitmapPixels = bilinearFiltering(zoomValue, zoomValue, bitmapWidth, bitmapHeight, bitmapPixels);
        } else {
            newBitmapPixels = trilinearFiltering(zoomValue, zoomValue, bitmapWidth, bitmapHeight, bitmapPixels);
        }
        int newBitmapWidth = Math.abs((int) (bitmapWidth * zoomValue));
        int newBitmapHeight = Math.abs((int) (bitmapHeight * zoomValue));

        //обновление currentBitmap
        MainActivity.currentBitmap = Bitmap.createBitmap(newBitmapPixels, newBitmapWidth, newBitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** поворачивает изображение */
    public static void rotation(double rotateAngle) {
        //инициализация переменных перед преобазованием
        rotateAngle = Math.toRadians(rotateAngle);
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

        //вычисление новых размеров
        int newBitmapWidth = (int)(bitmapHeight * Math.abs(Math.sin(rotateAngle))
                + bitmapWidth * Math.abs(Math.cos(rotateAngle)));
        int newBitmapHeight = (int)(bitmapHeight * Math.abs(Math.cos(rotateAngle))
                + bitmapWidth * Math.abs(Math.sin(rotateAngle)));

        //поворот currentBitmap
        int[] newBitmapPixels = rotatePixels(rotateAngle, bitmapWidth, bitmapHeight, newBitmapWidth, newBitmapHeight, bitmapPixels);
        MainActivity.currentBitmap = Bitmap.createBitmap(newBitmapPixels, newBitmapWidth, newBitmapHeight, Bitmap.Config.ARGB_8888);
        MainActivity.currentBitmap = killWhitespace(MainActivity.currentBitmap, newBitmapPixels, newBitmapWidth, newBitmapHeight);
    }

    /** поворачивает изображение на заданный угол */
    static int[] rotatePixels(double angle, int srcWidth, int srcHeight, int dstWidth, int dstHeight, int[] bitmapPixels) {
        //инициализация переменных
        int[] newBitmapPixels = new int[dstWidth*dstHeight];
        double oX = srcWidth/2, oY = srcHeight/2; //центр исходной картинки
        double dX = dstWidth/2 - oX; //сдвиг центра
        double dY = dstHeight/2 - oY; //сдвиг центра
        double xDiff, yDiff; int pixelA, pixelB, pixelC, pixelD, x, y,
                alphaA, greenA, blueA, redA, alphaB, greenB, blueB, redB,
                alphaC, greenC, blueC, redC, alphaD, greenD, blueD, redD,
                alphaNew, greenNew, blueNew, redNew;

        //вычисление новых пикселей
        for (int i = 0; i < dstHeight; i++) {
            for (int j = 0; j < dstWidth; j++) {
                //исходные координаты
                x = (int)(oX + (j-oX-dX) * Math.cos(angle) - (i-oY-dY) * Math.sin(angle));
                y = (int)(oY + (j-oX-dX) * Math.sin(angle) + (i-oY-dY) * Math.cos(angle));
                xDiff = (oX + (j-oX-dX) * Math.cos(angle) - (i-oY-dY) * Math.sin(angle))-x;
                yDiff = (oY + (j-oX-dX) * Math.sin(angle) + (i-oY-dY) * Math.cos(angle))-y;

                //если это пиксель исходной картинки, а не рамка
                if (y > 0 && x > 0 && y < srcHeight - 2 && x < srcWidth - 2) {
                    //что-то вроде билинейной фильтрации, т.к. координаты получаются нецелые
                    pixelA = bitmapPixels[(y) * srcWidth + (x)];
                    pixelB = bitmapPixels[(y) * srcWidth + (x + 1)];
                    pixelC = bitmapPixels[(y + 1) * srcWidth + (x)];
                    pixelD = bitmapPixels[(y + 1) * srcWidth + (x + 1)];

                    alphaA = pixelA >>> 24;
                    redA = (pixelA & 0x00ff0000) / 65536;
                    greenA = (pixelA & 0x0000ff00) / 256;
                    blueA = (pixelA & 0x000000ff);

                    alphaB = pixelB >>> 24;
                    redB = (pixelB & 0x00ff0000) / 65536;
                    greenB = (pixelB & 0x0000ff00) / 256;
                    blueB = (pixelB & 0x000000ff);

                    alphaC = pixelC >>> 24;
                    redC = (pixelC & 0x00ff0000) / 65536;
                    greenC = (pixelC & 0x0000ff00) / 256;
                    blueC = (pixelC & 0x000000ff);

                    alphaD = pixelD >>> 24;
                    redD = (pixelD & 0x00ff0000) / 65536;
                    greenD = (pixelD & 0x0000ff00) / 256;
                    blueD = (pixelD & 0x000000ff);

                    alphaNew = (int)(alphaA*(1-xDiff)*(1-yDiff) + alphaB*(xDiff)*(1-yDiff) + alphaC*(1-xDiff)*(yDiff) + alphaD*(xDiff)*(yDiff));
                    redNew   = (int)(redA  *(1-xDiff)*(1-yDiff) + redB  *(xDiff)*(1-yDiff) + redC  *(1-xDiff)*(yDiff) + redD  *(xDiff)*(yDiff));
                    greenNew = (int)(greenA*(1-xDiff)*(1-yDiff) + greenB*(xDiff)*(1-yDiff) + greenC*(1-xDiff)*(yDiff) + greenD*(xDiff)*(yDiff));
                    blueNew  = (int)(blueA *(1-xDiff)*(1-yDiff) + blueB *(xDiff)*(1-yDiff) + blueC *(1-xDiff)*(yDiff) + blueD *(xDiff)*(yDiff));
                    newBitmapPixels[i * dstWidth + j] = alphaNew * 16777216 + redNew * 65536 + greenNew * 256 + blueNew;
                } else {
                    //если это пиксель рамки
                    newBitmapPixels[i * dstWidth + j] = 0x00FFFFFF;
                }
            }
        }
        return newBitmapPixels;
    }

    /** сдвигает изображение на заданные координаты */
    static int[] movePixels(double xMove, double yMove, int bitmapWidth, int bitmapHeight, int[] bitmapPixels) {
        //инициализация переменных
        int moveX = (int) xMove;
        int moveY = (int) yMove;
        int[] newBitmapPixels = new int[bitmapWidth*bitmapHeight];

        //вычисление новых адресов пикселей и их перемещение
        for (int i = 0; i < bitmapHeight; i++) {
            for (int j = 0; j < bitmapWidth; j++) {
                if (i - moveY < bitmapHeight && i - moveY >= 0 && j - moveX < bitmapWidth && j - moveX >= 0) {
                    newBitmapPixels[i*bitmapWidth+j] = bitmapPixels[(i - moveY)*bitmapWidth + (j - moveX)];
                } else {
                    newBitmapPixels[i*bitmapWidth+j] = 0x00FFFFFF; //если это рамка
                }
            }
        }
        return newBitmapPixels;
    }

    /** убирает белые поля */
    static Bitmap killWhitespace(Bitmap srcBitmap, int[] bitmapPixels, int bitmapWidth, int bitmapHeight) {
        int left = 0, right = 0, up = 0, bottom = 0, i, j;

        //убираем сверху
        for (i = 0; i < bitmapHeight; i++) {
            for (j = 0; j < bitmapWidth; j++) {
                if(Color.alpha(bitmapPixels[i*bitmapWidth+j]) != 0) {
                    break;
                }
            }
            if (j == bitmapWidth) up++;
            else break;
        }

        //убираем снизу
        for (i = bitmapHeight-1; i >= 0; i--) {
            for (j = 0; j < bitmapWidth; j++) {
                if(Color.alpha(bitmapPixels[i*bitmapWidth+j]) != 0) {
                    break;
                }
            }
            if (j == bitmapWidth) bottom++;
            else break;
        }

        //убираем слева
        for (i = 0; i < bitmapWidth; i++) {
            for (j = 0; j < bitmapHeight; j++) {
                if(Color.alpha(bitmapPixels[j*bitmapWidth+i]) != 0) {
                    break;
                }
            }
            if (j == bitmapHeight) left++;
            else break;
        }

        //убираем справа
        for (i = bitmapWidth-1; i >= 0; i--) {
            for (j = 0; j < bitmapHeight; j++) {
                if(Color.alpha(bitmapPixels[j*bitmapWidth+i]) != 0) {
                    break;
                }
            }
            if (j == bitmapHeight) right++;
            else break;
        }

        return Bitmap.createBitmap(srcBitmap, left, up, bitmapWidth - left - right, bitmapHeight - up - bottom);
    }

    /** растягивает изображение, применяя билинейную фильтрацию */
    static int[] bilinearFiltering(double stretchX, double stretchY, int bitmapWidth, int bitmapHeight, int[] bitmapPixels) {
        //перевороты изображения
        if (stretchX < 0) {
            bitmapPixels = verticalFlip(bitmapWidth, bitmapHeight, bitmapPixels);
            stretchX = -stretchX;
        }
        if (stretchY < 0) {
            bitmapPixels = horizontalFlip(bitmapWidth, bitmapHeight, bitmapPixels);
            stretchY = -stretchY;
        }

        //инициализация переменных
        int newBitmapWidth = (int)(bitmapWidth * stretchX);
        int newBitmapHeight = (int)(bitmapHeight * stretchY);
        int[] newBitmapPixels = new int[newBitmapWidth * newBitmapHeight];
        double x_diff, y_diff; int pixelA, pixelB, pixelC, pixelD, x, y,
                alphaA, greenA, blueA, redA, alphaB, greenB, blueB, redB,
                alphaC, greenC, blueC, redC, alphaD, greenD, blueD, redD,
                alphaNew, greenNew, blueNew, redNew;
        stretchX = 1 / stretchX; stretchY = 1 / stretchY;

        //вычисление пикселей
        for (int i = 0; i < newBitmapHeight; i++) {
            for (int j = 0; j < newBitmapWidth; j++) {
                y = (int)(stretchY * i);
                x = (int)(stretchX * j);
                x_diff = (stretchX*j)-x;
                y_diff = (stretchY*i)-y;

                pixelA = bitmapPixels[y*bitmapWidth+x];
                pixelB = bitmapPixels[y*bitmapWidth+(x+1)%bitmapWidth];
                pixelC = bitmapPixels[((y+1)%bitmapHeight)*bitmapWidth+x];
                pixelD = bitmapPixels[((y+1)%bitmapHeight)*bitmapWidth+(x+1)%bitmapWidth];

                alphaA = pixelA >>> 24; redA = (pixelA & 0x00ff0000) / 65536; greenA = (pixelA & 0x0000ff00) / 256; blueA = (pixelA & 0x000000ff);
                alphaB = pixelB >>> 24; redB = (pixelB & 0x00ff0000) / 65536; greenB = (pixelB & 0x0000ff00) / 256; blueB = (pixelB & 0x000000ff);
                alphaC = pixelC >>> 24; redC = (pixelC & 0x00ff0000) / 65536; greenC = (pixelC & 0x0000ff00) / 256; blueC = (pixelC & 0x000000ff);
                alphaD = pixelD >>> 24; redD = (pixelD & 0x00ff0000) / 65536; greenD = (pixelD & 0x0000ff00) / 256; blueD = (pixelD & 0x000000ff);

                alphaNew = (int)(alphaA*(1-x_diff)*(1-y_diff) + alphaB*(x_diff)*(1-y_diff) + alphaC*(1-x_diff)*(y_diff) + alphaD*(x_diff)*(y_diff));
                redNew =   (int)(redA  *(1-x_diff)*(1-y_diff) + redB  *(x_diff)*(1-y_diff) + redC  *(1-x_diff)*(y_diff) + redD  *(x_diff)*(y_diff));
                greenNew = (int)(greenA*(1-x_diff)*(1-y_diff) + greenB*(x_diff)*(1-y_diff) + greenC*(1-x_diff)*(y_diff) + greenD*(x_diff)*(y_diff));
                blueNew =  (int)(blueA *(1-x_diff)*(1-y_diff) + blueB *(x_diff)*(1-y_diff) + blueC *(1-x_diff)*(y_diff) + blueD *(x_diff)*(y_diff));
                newBitmapPixels[i*newBitmapWidth+j] = alphaNew * 16777216 + redNew * 65536 + greenNew * 256 + blueNew;
            }
        }
        return newBitmapPixels;
    }

    /** растягивает изображение, применяя трилинейную фильтрацию */
    static int[] trilinearFiltering(double stretchX, double stretchY, int bitmapWidth, int bitmapHeight, int[] bitmapPixels) {
        //перевороты изображения
        if (stretchX < 0) {
            bitmapPixels = verticalFlip(bitmapWidth, bitmapHeight, bitmapPixels);
            stretchX = -stretchX;
        }
        if (stretchY < 0) {
            bitmapPixels = horizontalFlip(bitmapWidth, bitmapHeight, bitmapPixels);
            stretchY = -stretchY;
        }

        //вычисление необходимых исходных изображений
        double pow = 1;
        while (pow*pow > stretchX*stretchY) {
            pow /= 2;
        }
        int[] smallSrc = bilinearFiltering(pow, pow, bitmapWidth, bitmapHeight, bitmapPixels);
        int[] largeSrc = bilinearFiltering(pow*2, pow*2, bitmapWidth, bitmapHeight, bitmapPixels);

        //инициализация переменных
        int smallWidth = (int) (bitmapWidth * pow);
        int smallHeight = (int) (bitmapHeight * pow);
        int largeWidth = (int) (bitmapWidth * pow * 2);
        int largeHeight = (int) (bitmapHeight * pow * 2);
        int newBitmapWidth = (int) (bitmapWidth * stretchX);
        int newBitmapHeight = (int) (bitmapHeight * stretchY);
        int[] newBitmapPixels = new int[newBitmapWidth*newBitmapHeight];

        double largeWidthRatio  = ((float)(largeWidth -1 )) / newBitmapWidth;
        double largeHeightRatio = ((float)(largeHeight-1 )) / newBitmapHeight;
        double smallWidthRatio  = ((float)(smallWidth -1 )) / newBitmapWidth;
        double smallHeightRatio = ((float)(smallHeight-1 )) / newBitmapHeight;
        double hDiff = (largeWidth - newBitmapWidth)/(float)(largeWidth - smallWidth);

        double largeX, largeY, xDiffLarge, yDiffLarge, smallX, smallY, xDiffSmall, yDiffSmall;
        int pixelA, pixelB, pixelC, pixelD, pixelE, pixelF, pixelG, pixelH, largeIndex, smallIndex, newIndex,
            alphaA, redA, greenA, blueA, alphaB, redB, greenB, blueB,
            alphaC, redC, greenC, blueC, alphaD, redD, greenD, blueD,
            alphaE, redE, greenE, blueE, alphaF, redF, greenF, blueF,
            alphaG, redG, greenG, blueG, alphaH, redH, greenH, blueH,
            alphaNew, redNew, greenNew, blueNew;

        //вычисление пикселей
        for (int i = 0; i < newBitmapHeight; i++) {
            for (int j = 0; j < newBitmapWidth; j++) {
                //номер нового пикселя
                newIndex = i*newBitmapWidth + j;

                //получение пикселей большого исходника
                largeX = largeWidthRatio * j;
                largeY = largeHeightRatio * i;
                xDiffLarge = largeX - (int)largeX;
                yDiffLarge = largeY - (int)largeY;
                largeIndex = (int)largeY*largeWidth+(int)largeX;
                pixelA = largeSrc[largeIndex];
                pixelB = largeSrc[(largeIndex+1)%(largeWidth*largeHeight)];
                pixelC = largeSrc[(largeIndex+largeWidth)%(largeWidth*largeHeight)];
                pixelD = largeSrc[(largeIndex+largeWidth+1)%(largeWidth*largeHeight)];

                //получение пикселей маленького исходника
                smallX = smallWidthRatio * j;
                smallY = smallHeightRatio * i;
                xDiffSmall = smallX - (int)smallX;
                yDiffSmall = smallY - (int)smallY;
                smallIndex = (int)smallY*smallWidth+(int)smallX;
                pixelE = smallSrc[smallIndex];
                pixelF = smallSrc[(smallIndex+1)%(smallWidth*smallHeight)];
                pixelG = smallSrc[(smallIndex+smallWidth)%(smallWidth*smallHeight)];
                pixelH = smallSrc[(smallIndex+smallWidth+1)%(smallWidth*smallHeight)];

                alphaA = pixelA >>> 24; redA = (pixelA & 0x00ff0000) / 65536; greenA = (pixelA & 0x0000ff00) / 256; blueA = (pixelA & 0x000000ff);
                alphaB = pixelB >>> 24; redB = (pixelB & 0x00ff0000) / 65536; greenB = (pixelB & 0x0000ff00) / 256; blueB = (pixelB & 0x000000ff);
                alphaC = pixelC >>> 24; redC = (pixelC & 0x00ff0000) / 65536; greenC = (pixelC & 0x0000ff00) / 256; blueC = (pixelC & 0x000000ff);
                alphaD = pixelD >>> 24; redD = (pixelD & 0x00ff0000) / 65536; greenD = (pixelD & 0x0000ff00) / 256; blueD = (pixelD & 0x000000ff);
                alphaE = pixelE >>> 24; redE = (pixelE & 0x00ff0000) / 65536; greenE = (pixelE & 0x0000ff00) / 256; blueE = (pixelE & 0x000000ff);
                alphaF = pixelF >>> 24; redF = (pixelF & 0x00ff0000) / 65536; greenF = (pixelF & 0x0000ff00) / 256; blueF = (pixelF & 0x000000ff);
                alphaG = pixelG >>> 24; redG = (pixelG & 0x00ff0000) / 65536; greenG = (pixelG & 0x0000ff00) / 256; blueG = (pixelG & 0x000000ff);
                alphaH = pixelH >>> 24; redH = (pixelH & 0x00ff0000) / 65536; greenH = (pixelH & 0x0000ff00) / 256; blueH = (pixelH & 0x000000ff);

                alphaNew = (int)
                   (alphaA * (1-xDiffLarge) * (1-yDiffLarge) * (1-hDiff) +
                    alphaB *  (xDiffLarge)  * (1-yDiffLarge) * (1-hDiff) +
                    alphaC * (1-xDiffLarge) *  (yDiffLarge)  * (1-hDiff) +
                    alphaD *  (xDiffLarge)  *  (yDiffLarge)  * (1-hDiff) +
                    alphaE * (1-xDiffSmall) * (1-yDiffSmall) *  (hDiff)  +
                    alphaF *  (xDiffSmall)  * (1-yDiffSmall) *  (hDiff)  +
                    alphaG * (1-xDiffSmall) *  (yDiffSmall)  *  (hDiff)  +
                    alphaH *  (xDiffSmall)  *  (yDiffSmall)  *  (hDiff));
                redNew = (int)
                   (redA * (1-xDiffLarge) * (1-yDiffLarge) * (1-hDiff) +
                    redB *  (xDiffLarge)  * (1-yDiffLarge) * (1-hDiff) +
                    redC * (1-xDiffLarge) *  (yDiffLarge)  * (1-hDiff) +
                    redD *  (xDiffLarge)  *  (yDiffLarge)  * (1-hDiff) +
                    redE * (1-xDiffSmall) * (1-yDiffSmall) *  (hDiff)  +
                    redF *  (xDiffSmall)  * (1-yDiffSmall) *  (hDiff)  +
                    redG * (1-xDiffSmall) *  (yDiffSmall)  *  (hDiff)  +
                    redH *  (xDiffSmall)  *  (yDiffSmall)  *  (hDiff));
                greenNew = (int)
                   (greenA * (1-xDiffLarge) * (1-yDiffLarge) * (1-hDiff) +
                    greenB *  (xDiffLarge)  * (1-yDiffLarge) * (1-hDiff) +
                    greenC * (1-xDiffLarge) *  (yDiffLarge)  * (1-hDiff) +
                    greenD *  (xDiffLarge)  *  (yDiffLarge)  * (1-hDiff) +
                    greenE * (1-xDiffSmall) * (1-yDiffSmall) *  (hDiff)  +
                    greenF *  (xDiffSmall)  * (1-yDiffSmall) *  (hDiff)  +
                    greenG * (1-xDiffSmall) *  (yDiffSmall)  *  (hDiff)  +
                    greenH *  (xDiffSmall)  *  (yDiffSmall)  *  (hDiff));
                blueNew = (int)
                   (blueA * (1-xDiffLarge) * (1-yDiffLarge) * (1-hDiff) +
                    blueB *  (xDiffLarge)  * (1-yDiffLarge) * (1-hDiff) +
                    blueC * (1-xDiffLarge) *  (yDiffLarge)  * (1-hDiff) +
                    blueD *  (xDiffLarge)  *  (yDiffLarge)  * (1-hDiff) +
                    blueE * (1-xDiffSmall) * (1-yDiffSmall) *  (hDiff)  +
                    blueF *  (xDiffSmall)  * (1-yDiffSmall) *  (hDiff)  +
                    blueG * (1-xDiffSmall) *  (yDiffSmall)  *  (hDiff)  +
                    blueH *  (xDiffSmall)  *  (yDiffSmall)  *  (hDiff));
                newBitmapPixels[newIndex] = (alphaNew * 16777216) + (redNew * 65536) + (greenNew * 256) + blueNew;
            }
        }
        return newBitmapPixels;
    }

    /** переворачивает изображение по вертикали */
    static int[] verticalFlip(int bitmapWidth, int bitmapHeight, int[] bitmapPixels) {
        int[] newBitmapPixels = new int[bitmapWidth * bitmapHeight];
        for (int i = 0; i < bitmapHeight; i++) {
            for (int j = 0; j < bitmapWidth; j++) {
                newBitmapPixels[i * bitmapWidth + j] = bitmapPixels[i * bitmapWidth + (bitmapWidth - 1 - j)];
            }
        }
        return newBitmapPixels;
    }

    /** переворачивает изображение по горизонтали */
    static int[] horizontalFlip(int bitmapWidth, int bitmapHeight, int[] bitmapPixels) {
        int[] newBitmapPixels = new int[bitmapWidth * bitmapHeight];
        for (int i = 0; i < bitmapHeight; i++) {
            for (int j = 0; j < bitmapWidth; j++) {
                newBitmapPixels[i * bitmapWidth + j] = bitmapPixels[(bitmapHeight - 1 - i) * bitmapWidth + j];
            }
        }
        return newBitmapPixels;
    }

    /** автоматическое улучшение контраста */
    static Bitmap contrastStretch() {
        //инициализация переменных
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth * bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        int min = 1000, max = -1000, result;
        double iDouble, resultDouble;
        int[] newColors = new int[256];

        //находим минимальный и максимальный пиксель по значению серого цвета
        for (int i = 0; i < bitmapWidth*bitmapHeight; i++){
            int red = (bitmapPixels[i] & 0x00ff0000) / 65536;
            int green = (bitmapPixels[i] & 0x0000ff00) / 256;
            int blue = (bitmapPixels[i] & 0x000000ff);
            int gray = (red + green + blue) / 3;

            if (gray < min) min = gray;
            if (gray > max) max = gray;
        }

        //нормализация
        int[] newBitmapPixels = new int[bitmapWidth * bitmapHeight];
        double minDouble = (double) min; double maxDouble = (double) max;
        minDouble = minDouble / 255.0; maxDouble = maxDouble / 255.0;

        //считаем по формуле новое значение для каждого возможного старого
        //в результате на картинке «исчезает» серый цвет, становится ближе к белому и черному
        for (int i = 0; i < 256; i++){
            iDouble = (double) i;
            resultDouble =  (iDouble - minDouble)/ (maxDouble - minDouble);
            result = (int)resultDouble;
            if (result < 0) result = 0;
            if (result > 255) result = 255;
            newColors[i] = result;
        }

        //обновление пикселей
        for (int i = 0; i < bitmapWidth*bitmapHeight; i++){
            int alpha = bitmapPixels[i] >>> 24;

            int oldRed = (bitmapPixels[i] & 0x00ff0000) / 65536;
            int oldGreen = (bitmapPixels[i] & 0x0000ff00) / 256;
            int oldBlue = (bitmapPixels[i] & 0x000000ff);

            int newRed = newColors[oldRed];
            int newGreen = newColors[oldGreen];
            int newBlue = newColors[oldBlue];

            newBitmapPixels[i] = (alpha*16777216) + (newRed * 65536) + (newGreen * 256) + (newBlue);
        }

        return Bitmap.createBitmap(newBitmapPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** перевод картинки в сепию */
    static Bitmap sepiaEffect() {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth * bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        int[] newBitmapPixels = new int[bitmapWidth * bitmapHeight];

        //обновление пикслей
        for (int i = 0; i < bitmapWidth * bitmapHeight; i++){
            int alpha = bitmapPixels[i] >>> 24;
            double oldRed = (double) ((bitmapPixels[i] & 0x00ff0000) / 65536);
            double oldGreen = (double) ((bitmapPixels[i] & 0x0000ff00) / 256);
            double oldBlue = (double) ((bitmapPixels[i] & 0x000000ff));

            int newRed = (int) (0.393*oldRed + 0.769*oldGreen + 0.189*oldBlue);
            int newGreen = (int) (0.349*oldRed + 0.686*oldGreen + 0.168*oldBlue);
            int newBlue = (int) (0.272*oldRed + 0.534*oldGreen + 0.131*oldBlue);

            if(newRed>255) newRed = 255;
            if(newGreen>255) newGreen = 255;
            if(newBlue>255) newBlue = 255;

            newBitmapPixels[i] = (alpha*16777216) | (newRed * 65536) | (newGreen * 256) | (newBlue);
        }

        return Bitmap.createBitmap(newBitmapPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** перевод картинки в негатив */
    static Bitmap negativeFilter() {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth * bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        int[] newBitmapPixels = new int[bitmapWidth*bitmapHeight];

        //обновление пикселей: берётся обратное значение, был минимум в каком-то канале - станет максимум
        for (int i = 0; i < bitmapWidth*bitmapHeight; i++){
            int alpha = bitmapPixels[i] >>> 24;
            int oldRed = (bitmapPixels[i] & 0x00ff0000) / 65536;
            int oldGreen = (bitmapPixels[i] & 0x0000ff00) / 256;
            int oldBlue = (bitmapPixels[i] & 0x000000ff);

            int newRed = 255 - oldRed;
            int newGreen = 255 - oldGreen;
            int newBlue = 255 - oldBlue;

            newBitmapPixels[i] = (alpha*16777216) + (newRed * 65536) + (newGreen * 256) + (newBlue);
        }
        return Bitmap.createBitmap(newBitmapPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** гамма-фильтр */
    static Bitmap gammaFilter(double gamma) {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        double resultDouble, iDouble;
        int result;
        double gammaLevel = 1.0 / gamma;
        int[] newColor = new int[256];

        //считаем по формуле новое значение для каждого возможного старого
        for (int i = 0; i < 256; i++){
            iDouble = (double) i;
            resultDouble = 255.0 * Math.pow(iDouble / 255.0, gammaLevel);
            result = (int) resultDouble;
            if (result < 0) result = 0;
            if (result > 255) result = 255;
            newColor[i] = result;
        }

        //обновляем пиксели
        int[] newPixels = new int[bitmapWidth*bitmapHeight];
        for (int i = 0; i < bitmapWidth*bitmapHeight; i++){
            int alpha = bitmapPixels[i] >>> 24;
            int oldRed = (bitmapPixels[i] & 0x00ff0000) / 65536;
            int oldGreen = (bitmapPixels[i] & 0x0000ff00) / 256;
            int oldBlue = (bitmapPixels[i] & 0x000000ff);

            int newRed = newColor[oldRed];
            int newGreen = newColor[oldGreen];
            int newBlue = newColor[oldBlue];

            newPixels[i] = (alpha*16777216) + (newRed * 65536) + (newGreen * 256) + (newBlue);
        }
        return Bitmap.createBitmap(newPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** фильтр Кувахары */
    static Bitmap kuwaharaFilter(int radius) {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth * bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        int[] newBitmapPixels = new int[bitmapWidth * bitmapHeight];

        //обновление пикселей
        for (int y = radius; y < bitmapHeight - radius; y++){ // Для текущего пикселя изображения
            for (int x = radius; x < bitmapWidth - radius; x++){ // Посчитать среднее значение цвета в зонах по бокам от пикселя
                if(Color.alpha(bitmapPixels[x + y * bitmapWidth]) != 0) {
                    //для каждого пикселя изображения считаем средние цвета в четырёх зонах
                    int[] curColor = new int[2];
                    curColor[1] = 256;

                    RGBmeanGrayMaxMin(bitmapPixels, curColor, x - radius, y - radius, x, y, bitmapWidth); //В зоне слева сверху

                    RGBmeanGrayMaxMin(bitmapPixels, curColor, x, y - radius, x + radius, y, bitmapWidth); // В зоне справа сверху

                    RGBmeanGrayMaxMin(bitmapPixels, curColor, x, y, x + radius, y + radius, bitmapWidth); // В зоне справа снизу

                    RGBmeanGrayMaxMin(bitmapPixels, curColor, x - radius, y, x, y + radius, bitmapWidth); // В зоне слева снизу

                    //присвоить значение среднего цвета той зоны, в которой значение серого отличается сильнее всего
                    curColor[0] += (bitmapPixels[x + y * bitmapWidth] >>> 24)* 16777216;
                    newBitmapPixels[x + y * bitmapWidth] = curColor[0];
                }
            }
        }
        return Bitmap.createBitmap(newBitmapPixels, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    /** подсчёт среднего и дисперсии для зоны вокруг пикселя для Кувахары */
    static void RGBmeanGrayMaxMin(int[] pixels, int[] color, int x1, int y1, int x2, int y2, int w) {
        int max = -1, min = 256;
        int redMean = 0, greenMean = 0, blueMean = 0, count = 0;

        //Вычисление среднего значения каждого канала и значения серого цвета
        for(int x = x1; x <= x2;x++){
            for(int y = y1; y <= y2; y++){
                if (Color.alpha(pixels[x + y*w]) != 0) { //игнорируем рамки, возникшие при повороте
                    int red =  (pixels[x + y * w] & 0x00ff0000) / 65536;
                    int green =(pixels[x + y * w] & 0x0000ff00) / 256;
                    int blue = (pixels[x + y * w] & 0x000000ff);

                    redMean = redMean + red;
                    greenMean = greenMean + green;
                    blueMean = blueMean + blue;

                    int variance = (red + green + blue) / 3;
                    if (variance < min) min = variance;
                    if (variance > max) max = variance;
                    count++;
               }
            }
        }

        //сохранение результатов
        int var = max - min;
        if(var < color[1]) {
            if (count != 0) {
                int newR = redMean / count;
                int newG = greenMean / count;
                int newB = blueMean / count;

                color[0] = (newR * 65536) + (newG * 256) + (newB);
                color[1] = max - min;
            } else { //если это целиком часть рамки
                color[0] = 0x00FFFFFF;
                color[1] = 0;
            }
        }
    }

    /** нерезкое маскирование */
    static Bitmap unsharpMasking(int radius, double amount, int threshold) {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] pixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(pixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

        int matrixSize = 2 * radius + 1;
        double sum = 0;
        double matrix[][] = new double[matrixSize][matrixSize];
        int newRed, newGreen, newBlue, x1=0, y1=0;
        double sumRed, sumGreen, sumBlue;
        
        Bitmap result = Bitmap.createBitmap(bitmapWidth - matrixSize + 1, bitmapHeight - matrixSize + 1, Bitmap.Config.ARGB_8888);
        
        // заполнение матрицы коэффициентов по функции Гаусса
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                double xDouble = (double)x, yDouble = (double)y, radiusDouble = (double)radius;
                matrix[x1][y1] =  (Math.pow (Math.E, -((xDouble*xDouble+xDouble*xDouble)/(2*radiusDouble*radiusDouble))))
                        / (2*Math.PI*radiusDouble*radiusDouble);
                sum += matrix[x1][y1];
                y1++;
            }
            y1=0;
            x1++;
        }

        if (sum == 0)
            sum = 1;

        for (int y = 0; y < bitmapHeight - matrixSize + 1; y++) {
            for (int x = 0; x < bitmapWidth - matrixSize + 1; x++) {
                if ((pixels[x + bitmapWidth * y] >>> 24) != 0) { //если не часть рамки
                    sumRed = sumGreen = sumBlue = 0;
                    
                    // работа с матрицей конволюции
                    int counter = 0;
                    for (int i = 0; i < matrixSize; ++i) {
                        for (int j = 0; j < matrixSize; ++j) {
                            if ((pixels[x + i + bitmapWidth * (y + j)] >>> 24) != 0) {
                                sumRed   += (double)(Color.red  (pixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                sumGreen += (double)(Color.green(pixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                sumBlue  += (double)(Color.blue (pixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                counter++;
                            }
                        }
                    }
                    double areaShare = (matrixSize*matrixSize)/counter;

                    // получение финального красного цвета
                    newRed = (int) (sumRed * areaShare / sum);
                    if (newRed < 0) {
                        newRed = 0;
                    } else if (newRed > 255) {
                        newRed = 255;
                    }
                    // получение финального зелёного цвета
                    newGreen = (int) (sumGreen * areaShare / sum);
                    if (newGreen < 0) {
                        newGreen = 0;
                    } else if (newGreen > 255) {
                        newGreen = 255;
                    }
                    // получение финального синего цвета
                    newBlue = (int) (sumBlue * areaShare / sum);
                    if (newBlue < 0) {
                        newBlue = 0;
                    } else if (newBlue > 255) {
                        newBlue = 255;
                    }

                    // подсчёт разницы
                    int diff = (newRed - Color.red(pixels[(x + 1) + (y + 1) * bitmapWidth])
                            + newGreen - Color.green(pixels[(x + 1) + (y + 1) * bitmapWidth])
                            + newBlue - Color.blue(pixels[(x + 1) + (y + 1) * bitmapWidth])) / 3;

                    //подсчёт новых значений
                    if (Math.abs(2 * diff) > threshold) {
                        newRed   = Color.red  (pixels[(x + 1) + (y + 1) * bitmapWidth]) + (int)(diff * amount);
                        newGreen = Color.green(pixels[(x + 1) + (y + 1) * bitmapWidth]) + (int)(diff * amount);
                        newBlue  = Color.blue (pixels[(x + 1) + (y + 1) * bitmapWidth]) + (int)(diff * amount);

                        if (newRed > 255) newRed = 255;
                        if (newRed < 0) newRed = 0;
                        if (newGreen > 255) newGreen = 255;
                        if (newGreen < 0) newGreen = 0;
                        if (newBlue > 255) newBlue = 255;
                        if (newBlue < 0) newBlue = 0;
                    }

                    //запись в битмап
                    int alpha = pixels[x + bitmapWidth * y] >>> 24;
                    result.setPixel(x, y, Color.argb(alpha, newRed, newGreen, newBlue));
                }
            }
        }
        return result;
    }

    /** размытие по Гауссу */
    static Bitmap gaussianBlur(int radius) {
        //инициализация
        int bitmapWidth = MainActivity.currentBitmap.getWidth();
        int bitmapHeight = MainActivity.currentBitmap.getHeight();
        int[] bitmapPixels = new int[bitmapWidth*bitmapHeight];
        MainActivity.currentBitmap.getPixels(bitmapPixels, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);

        int matrixSize = 2 * radius + 1;
        double sum = 0;
        double matrix[][] = new double[matrixSize][matrixSize];
        int newRed, newGreen, newBlue, x1=0, y1=0;
        double sumRed, sumGreen, sumBlue;

        Bitmap result = Bitmap.createBitmap(bitmapWidth - matrixSize + 1, bitmapHeight - matrixSize + 1, Bitmap.Config.ARGB_8888);

        //заполнение матрицы коэффициентов по функции Гаусса
        for (int x = -radius; x <= radius; x++)
        {
            for (int y = -radius; y <= radius; y++)
            {
                double xDouble = (double)x, yDouble = (double)y, radiusDouble = (double)radius;
                matrix[x1][y1] =  (Math.pow (Math.E, -((xDouble*xDouble+yDouble*yDouble)/(2*radiusDouble*radiusDouble))))
                        / (2*Math.PI*radiusDouble*radiusDouble);
                sum += matrix[x1][y1];
                y1++;
            }
            y1=0;
            x1++;
        }

        if (sum == 0)
            sum = 1;

        for (int y = 0; y < bitmapHeight - matrixSize + 1; y++) {
            for (int x = 0; x < bitmapWidth - matrixSize + 1; x++) {
                if ((bitmapPixels[x + bitmapWidth * y] >>> 24) != 0) { //если не часть рамки
                    sumRed = sumGreen = sumBlue = 0;

                    // работа с матрицей конволюции
                    int counter = 0;
                    for (int i = 0; i < matrixSize; ++i) {
                        for (int j = 0; j < matrixSize; ++j) {
                            if ((bitmapPixels[x + i + bitmapWidth * (y + j)] >>> 24) != 0) {
                                sumRed   += (double)(Color.red  (bitmapPixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                sumGreen += (double)(Color.green(bitmapPixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                sumBlue  += (double)(Color.blue (bitmapPixels[x + i + bitmapWidth * (y + j)])) * matrix[i][j];
                                counter++;
                            }
                        }
                    }
                    double areaShare = (matrixSize*matrixSize)/counter;
                    // получение финального красного цвета
                    newRed = (int) (sumRed * areaShare / sum);
                    if (newRed < 0) {
                        newRed = 0;
                    } else if (newRed > 255) {
                        newRed = 255;
                    }
                    // получение финального зелёного цвета
                    newGreen = (int) (sumGreen * areaShare / sum);
                    if (newGreen < 0) {
                        newGreen = 0;
                    } else if (newGreen > 255) {
                        newGreen = 255;
                    }
                    // получение финального синего цвета
                    newBlue = (int) (sumBlue * areaShare / sum);
                    if (newBlue < 0) {
                        newBlue = 0;
                    } else if (newBlue > 255) {
                        newBlue = 255;
                    }

                    //запись в битмап
                    int alpha = bitmapPixels[x + bitmapWidth * y] >>> 24;
                    result.setPixel(x, y, Color.argb(alpha, newRed, newGreen, newBlue));
                }
            }
        }
        return result;
    }
}
